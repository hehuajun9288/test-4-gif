//
//  main.m
//  OverTimeExitDemo
//
//  Created by HO on 15/5/29.
//  Copyright (c) 2015年 com.gdlc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, @"ELCUIApplication", NSStringFromClass([AppDelegate class]));
    }
}
